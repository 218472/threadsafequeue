#include <iostream>
#include <thread>
#include <chrono>
#include "../ThreadSafeQueue.h"

static const auto NUMBER_OF_READING_THREADS = 2;
static const auto THREAD_DELAY_MS = 1'000;
static const auto QUEUE_VALIDITY_TIME_MS = 10'000;
static const auto NUMBER_OF_PUSHED_ELEMENTS = 1'000;
CThreadSafeQueue<unsigned> thread_safe_queue;
std::mutex logMutex;

auto pushThreadFunc = [&]()
{
    for(unsigned i = 0; i < NUMBER_OF_PUSHED_ELEMENTS; ++i)
    {
        thread_safe_queue.Push(i);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(QUEUE_VALIDITY_TIME_MS));
    thread_safe_queue.Invalidate();
};

auto popThreadFunc = [&]()
{
    std::thread::id thread_id;

    while(thread_safe_queue.IsValid())
    {
        if(!thread_safe_queue.Empty())
        {
            thread_id = std::this_thread::get_id();

            unsigned data = 0;
            if(thread_safe_queue.Pop(data))
            {
                std::lock_guard<std::mutex> guard(logMutex);
                std::cout << thread_id << " : " << data << std::endl << std::flush;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(THREAD_DELAY_MS));
        }
    }
};

int main()
{
    std::thread pushThread(pushThreadFunc);
    std::vector<std::thread> popThreads(NUMBER_OF_READING_THREADS);

    for(auto &thread : popThreads)
    {
        thread = std::thread(popThreadFunc);
    }

    pushThread.join();
    for(auto &thread : popThreads)
    {
        thread.join();
    }

    return 0;
}