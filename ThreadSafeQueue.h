#pragma once

#include<atomic>
#include<queue>
#include<mutex>
#include<condition_variable>

namespace
{
    using LockGuard_T = std::lock_guard<std::mutex>;
    using UniqueLock_T = std::unique_lock<std::mutex>;
};

template<class T>
class CThreadSafeQueue : std::queue<T>
{
public:
    CThreadSafeQueue() : _queueValid(true){}
    ~CThreadSafeQueue() { Invalidate(); }

    void Push(const T &data)
    {
        LockGuard_T Lock(_mutex);
        this->push(data);
        m_conditionVariable.notify_one();
    }

    bool TryPush(const T &data)
    {
        bool res = m_mutex.try_lock();

        if(res)
        {
            push(data);
            _mutex.unlock();
            _conditionVariable.notify_one();
        }

        return res;
    }

    bool Pop(T &data)
    {
        UniqueLock_T Lock(_mutex);

        m_conditionVariable.wait(lock, [this]()
        {
            return !this->empty() || !_queueValid;
        });

        if(!_queueValid)
        {
            return false;
        }

        data = this->front();
        this->pop();

        return true;
    }

    bool TryPop(T &data)
    {
        LockGuard_T Lock(_mutex);

        if(this->empty() || !_queueValid)
        {
            return false;
        }

        data = this->front();
        this->pop();

        return true;
    }

    bool Empty() const
    {
        LockGuard_T Lock(_mutex);
        return this->empty();
    }

    size_t Size() const
    {
        LockGuard_T Lock(_mutex);
        return this->size();
    }

    void Invalidate()
    {
        LockGuard_T Lock(_mutex);
        _queueValid = false;
        _conditionVariable.notify_all();
    }

    bool IsValid() const
    {
        LockGuard_T Lock(_mutex);
        return _queueValid;
    }

private:
    mutable std::mutex              _mutex;
    std::condition_variable         _conditionVariable;
    std::atomic<bool>               _queueValid;
};
